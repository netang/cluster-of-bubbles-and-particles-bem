% --------------------------------------------------------
% This script to write current calculation date in Arrays.
% --------------------------------------------------------

% saveas(handleGraphic, ['save_img/' int2str(hh) '.png'],'png');
% print(handleGraphic,'-dpng', '-r300', ['save_img/' int2str(hh) '.png']);

% [normals, areas] = get_normals_areas(v, facesb);

% [Bc] = get_centers(v,areas);
[Vg] = get_volume(rb, normalsb, areasb);
[Bc] = P3DBL_get_centers_mean(v);
% [Vg]=P3DBL_get_volume_all(v); % ������������ �������. ������ ��� ��������
% ������ �����, ����� ���������� ��������� = ���������� ������.

% [Bc] = get_centers(rb, areasb); % может можно удалить
% if mod(h,50) == 0
%     time_div_tmax = time/tmax;
%     save(['cluster_calc_three_part_five_particles_vertices/vertices_it' int2str(h) '.mat'], 'time_div_tmax', 'v')
% end

% VerticesAllTime(hh,:,:) = v;
Vg_mas(hh,:) = Vg./Vg0;
Rp_mas(hh,:,:) = Rp;
Up_mas(hh,:,:) = Up;
Bc_mas(hh,:,:) = Bc;
time_mas(hh) = time/tmax;
hh = hh + 1;