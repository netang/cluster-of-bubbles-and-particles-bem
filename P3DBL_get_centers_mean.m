function [Bc]=P3DBL_get_centers_mean(v)

global NB Nvert
for ib=1:NB
%     sui=find(indv==ib);
    vb=zeros(Nvert,3);
    vb=v( (ib-1)*Nvert+1:ib*Nvert,:);
    Bc(ib,1)=mean(vb(:,1));
    Bc(ib,2)=mean(vb(:,2));
    Bc(ib,3)=mean(vb(:,3));
end;

% Bc(1)=mean(v(:,1));
% Bc(2)=mean(v(:,2));
% Bc(3)=mean(v(:,3));