function h = P3DBL_get_mult_G_handle_GPU(vertices, normals, areas, G_singular, dGn_singular)
h = @mult;
    function result = mult(f)
         [result] = Overload_Multiplication_Operator(f, vertices, normals, areas, G_singular, dGn_singular);
    end
end 
 