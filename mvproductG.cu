#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

__global__ void mvproductG(double *res,  const double *x, 
                    const double *v, const double *areas, const int Nv) 
{
    int indx = blockIdx.x * blockDim.x + threadIdx.x;
    int i;

    double v1,v2,v3,vm1,vm2,vm3,d,c;
    
    c=1/(4*M_PI);
    
    if(indx<Nv)
    {   res[indx]=0;
        vm1=v[indx*3];
        vm2=v[indx*3+1];
        vm3=v[indx*3+2];
		for(i=0; i<Nv; i++)
		    {if (i==indx) continue;
		     v1=v[i*3]-vm1;
			 v2=v[i*3+1]-vm2;
			 v3=v[i*3+2]-vm3;
			 d=rsqrt(v1*v1+v2*v2+v3*v3);

			 res[indx]+=c*x[i]*areas[i]*d;
			  __syncthreads();
		    }

    }  
    
}

