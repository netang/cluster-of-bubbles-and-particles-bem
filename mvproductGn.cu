#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

__global__ void mvproductGn(double *res,  const double *x, 
                    const double *v, const double *nor, const double *areas, const int Nv) 
{
    int indx = blockIdx.x * blockDim.x + threadIdx.x;
    int i;

    double v1,v2,v3,vm1,vm2,vm3,d,c,n1,n2,n3,rn;
    
    c=1/(4*M_PI);
    
    if(indx<Nv)
    {   res[indx]=0;
        vm1=v[indx*3];
        vm2=v[indx*3+1];
        vm3=v[indx*3+2];
		for(i=0; i<Nv; i++)
		    {if (i==indx) continue;
		     v1=-v[i*3]+vm1;
			 v2=-v[i*3+1]+vm2;
			 v3=-v[i*3+2]+vm3;
			 d=rsqrt(v1*v1+v2*v2+v3*v3);
			 n1=nor[i*3  ];
			 n2=nor[i*3+1];
			 n3=nor[i*3+2];
			 rn=(v1*n1+v2*n2+v3*n3);

			 res[indx]+=c*x[i]*areas[i]*d*d*d*rn;
			  __syncthreads();
		    }

    }  
    
}

