function [ BubbleCenters, ParticleCenters, BubbleRadius, ParticleRadius  ] = GenerateBPposition_for_3x3scheme( r0, d )
% ----------------------------
% This function generate bubbles and particle positions and them initial radii for 3x3 scheme
% r0 -- radii for bubbles and particles
% d  -- distance between centers of neighboring objects
% ----------------------------

BubbleCenters = [   -d       d       0;
                    -d       0       0;
                    -d      -d       0;
                     0       d       0; 
                     0      -d       0;
                     d       d       0;
                     d       0       0;
                     d      -d       0; ];
BubbleRadius   = [r0 r0 r0 r0 r0 r0 r0 r0];

ParticleCenters = [0 0 0];
ParticleRadius  = [r0];

end

