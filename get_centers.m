function [Bc] = get_centers(v, areas)

global NB Nvert
S_bub = zeros(NB, 1);
Bc = zeros(NB, 3);


for i=1:NB
    k=(Nvert*(i-1)+1:Nvert*i)';
    Bc(i,1)=sum(v(k,1).*areas(k));
    Bc(i,2)=sum(v(k,2).*areas(k));
    Bc(i,3)=sum(v(k,3).*areas(k));
    S_bub(i) = sum(areas(k));
end;
Bc(:,1)=Bc(:,1)./S_bub;
Bc(:,2)=Bc(:,2)./S_bub;
Bc(:,3)=Bc(:,3)./S_bub;


% for ib=1:Nbub
%     sui=find(indv==ib);
%     vb=zeros(nodeBN,3);
%     vb=v(sui,:);    
%     Bc(ib,1)=mean(vb(:,1));
%     Bc(ib,2)=mean(vb(:,2));
%     Bc(ib,3)=mean(vb(:,3));
% end;

% Bc(1)=mean(v(:,1));
% Bc(2)=mean(v(:,2));
% Bc(3)=mean(v(:,3));