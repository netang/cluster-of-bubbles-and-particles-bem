function result = Overload_Multiplication_Operator( x, vertices, normals, areas, G_singular, dGn_singular)
%OVERLOADMULTIPLICATIONOPERATOR Summary of this function goes here
%   Detailed explanation goes here


global NBV NPV NP Nvert
global S
global rho_l m_p
% ==== global variable for time measurement =============
global LHS_MVP_GPU_time myhh singular_element_of_LHS_time
global gpuArray_LHS_time
% =======================================================


v_gpu       = reshape(vertices', 1, (NBV+NPV)*3);
normals_gpu = reshape(normals', 1, (NBV+NPV)*3);


entrypoint = parallel.gpu.CUDAKernel('BEM_LHS.ptx', 'BEM_LHS.cu');
entrypoint.ThreadBlockSize = S;
entrypoint.GridSize = ceil( (NBV + NPV)/S );

tic;
lhs_gpu  = gpuArray( zeros(NBV + NPV,1) );   % 
v_gpu    = gpuArray(v_gpu); % ����
ar_gpu   = gpuArray(areas); % ����
normals_gpu  = gpuArray(normals_gpu);
x_gpu = gpuArray(x);
gpuArray_LHS_time(myhh) = toc;

tic;
[lhs_gpu]    = feval(entrypoint, lhs_gpu, x_gpu, v_gpu, normals_gpu, ar_gpu, rho_l/m_p, NBV, NPV, NP, Nvert);


result1  = gather(lhs_gpu); % �������� rhs_gpu � res ���� ����� � ���� ����������, ������� ��� ����
LHS_MVP_GPU_time(myhh) = toc;


tic;
right_integral = zeros(0, 3);

right_integral_parts = [x(NBV+1:end).*normals(NBV+1:end, 1).*areas(NBV+1:end)  ...
                        x(NBV+1:end).*normals(NBV+1:end, 2).*areas(NBV+1:end)  ...
                        x(NBV+1:end).*normals(NBV+1:end, 3).*areas(NBV+1:end)];
for i = 1:Nvert:NPV
    right_integral = [right_integral; sum( right_integral_parts(i:(i-1)+Nvert, :), 1)];
end
right_integral = kron(right_integral, ones(Nvert, 1));
% size_right_integral_parts = size(right_integral_parts)
% size_right_integral = size(right_integral)


Qp_singular = dot(normals(NBV+1:end,:), right_integral, 2 ) .* G_singular(NBV+1:end)';
% size(Qp_singular)


result2  = [ 
                (G_singular(1:NBV)'.*x(1:NBV)); % singular part for Lb, x(1:NBV) is qb
                -(dGn_singular(NBV+1:end) - 1/2).*x(NBV+1:end) + (rho_l/m_p*Qp_singular); % singular part for Qp x(NBV+1:end) is phip
           ];

result = result1 + result2; 
singular_element_of_LHS_time(myhh) = toc;





end

