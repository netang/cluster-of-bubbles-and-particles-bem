function [Gns] = get_singular_dGn(v, normals, areas)
global S;
global myhh singular_GnS_time
tic;
Nvert=length(v);
Nvert3=Nvert*3;
Gns=zeros(1,Nvert);
f=ones(Nvert,1);         
tempGn = zeros(Nvert,1);
result=zeros(Nvert,1);
v_gpu=reshape(v',1,Nvert3);
nor_gpu=reshape(normals',1,Nvert3);

N=Nvert;
entrypoint = parallel.gpu.CUDAKernel('mvproductGn.ptx', 'mvproductGn.cu');
entrypoint.ThreadBlockSize=S;
entrypoint.GridSize = ceil(N/S);
tempGn=gpuArray(tempGn);
v_gpu=gpuArray(v_gpu);
ar_gpu=gpuArray(areas);
nor_gpu=gpuArray(nor_gpu);
f_gpu=gpuArray(f);


[tempGn] = feval(entrypoint, tempGn, f_gpu, v_gpu, nor_gpu, ar_gpu, N);
result=-0.5-gather(tempGn);
Gns=result; 

singular_GnS_time(myhh) = toc;