function [Pg0, phog0, mg0] = get_init_parameter(rb, BubbleRadius)

% global P0 a T0 sigma Rg indv
global P0 T0 sigma Rg

% Pg0 = P0+2*sigma./a(indv);
Pg0 = P0+2*sigma./BubbleRadius;
% Rg
% T0
% Pg0
phog0 = Pg0/(Rg*T0);
mg0 = 4*pi*BubbleRadius.^3.*phog0/3;



