#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

__global__ void BEM_RHS(      double * res, 
                        const double * phib, 
                        const double * vertices,
                        const double * normals,
                        const double * areas,
                        const double * Up,
                        const int NBvert,
                        const int NPvert,
                        const int NP,
                        const int Nvert,
                              double * mass) 
{
    int indx = blockIdx.x * blockDim.x + threadIdx.x;
    int i, k, j, i3;

    double v1, v2, v3, y1, y2, y3, d, c, n1, n2, n3, scalar_product_rn, GdS;
    
    c = 1/(4*M_PI); /* coefficient for Green function */
    /* -------------------- */
    if(indx < (NBvert + NPvert) )
    {
        res[indx] = 0.0;
        y1 = vertices[indx*3];
        y2 = vertices[indx*3+1];
        y3 = vertices[indx*3+2];
        for( i = 0; i < NBvert; i++  )  /* {i} for bubbles (Mb) */
        {
            if(i == indx) continue;
            k = i*3;
            v1 = y1 - vertices[k];
            v2 = y2 - vertices[k+1];
            v3 = y3 - vertices[k+2];
            d = rsqrt(v1*v1 + v2*v2 + v3*v3);
            n1 = normals[k  ];
            n2 = normals[k+1];
            n3 = normals[k+2];
            scalar_product_rn = (v1*n1 + v2*n2 + v3*n3);
            res[indx] += phib[i]*c*d*d*d*scalar_product_rn*areas[i];
            /* __syncthreads(); */
        }
        for( j = 0; j < NP; j++ )
        {
            k = j*3;
            mass[k]   = 0.0;
            mass[k+1] = 0.0;
            mass[k+2] = 0.0;
            for( i = NBvert+j*Nvert; i < NBvert+(j+1)*Nvert; i++ )
            {
                if(i == indx) continue;
                v1 = y1 - vertices[i*3];
                v2 = y2 - vertices[i*3+1];
                v3 = y3 - vertices[i*3+2];
                d = rsqrt(v1*v1 + v2*v2 + v3*v3);
                GdS = c*d*areas[i];
                mass[k]   += normals[i*3  ]*GdS;
                mass[k+1] += normals[i*3+1]*GdS;
                mass[k+2] += normals[i*3+2]*GdS;               
            }
            res[indx] -= ( mass[k]*Up[k] + mass[k+1]*Up[k+1] + mass[k+2]*Up[k+2] );
        }
    }
    /* -------------------- */
    
 
    
}

