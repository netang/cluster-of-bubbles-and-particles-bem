function [ output_args ] = MyFriendSubPlot( time_mas, Vg_mas, Bc_mas, Rp_mas, Up_mas,...
    stime_mas, sVg_mas, sBc_mas, sRp_mas, sUp_mas )
%MYFRIENDSUBPLOT Моя функция-помощник, subplot'ит

figure;
subplot(2,2,1);
plot( time_mas, Vg_mas(:,1).^(1/3), stime_mas, sVg_mas(:,1).^(1/3) );
title('Volume dynamics');

% subplot(2,2,2);
% plot( time_mas, Rp_mas(:,1), stime_mas, sRp_mas(:,1) );
% title('Particle center dynamics');

subplot(2,2,3);
plot( time_mas, Bc_mas(:,1), stime_mas, sBc_mas(:,1) );
title('Bubble center dynamics');

% subplot(2,2,4);
% plot( time_mas, Up_mas(:,1), stime_mas, sUp_mas(:,1) )
% title('Up dynamics');

end

