function result = get_BEM_rhs( phib, vertices, normals, areas, dGn_singular, G_singular, Up )
%GET_BEM_RHS is function to calculate right hand side for BEM equation and
% return it

% tic;

global NBV NPV Nvert NP NB
global S
% === for time measurement ========
global myhh
global RHS_CPU_time
global RHS_GPU_time
global LHS_MVP_CPU_time 
global LHS_MVP_GPU_time
global BEM_matrix_time
global Jp_matrix_time
global singular_element_of_RHS_time
global singular_element_of_LHS_time
global gpuArray_RHS_time
global gpuArray_LHS_time
% =================================

rhs     = zeros(NBV + NPV,1);

v_gpu    = reshape(vertices', 1, (NBV+NPV)*3 );
nor_gpu  = reshape(normals', 1, (NBV+NPV)*3 );
Up_gpu   = reshape(Up', 1, NP*3);

entrypoint = parallel.gpu.CUDAKernel('BEM_RHS.ptx', 'BEM_RHS.cu');
entrypoint.ThreadBlockSize = S;
entrypoint.GridSize = ceil( (NBV + NPV)/S );

tic;
rhs_gpu  = gpuArray(rhs);
v_gpu    = gpuArray(v_gpu);
ar_gpu   = gpuArray(areas);
nor_gpu  = gpuArray(nor_gpu);
phib_gpu = gpuArray(phib);
Up_gpu   = gpuArray(Up_gpu);
mass_n_GdS = gpuArray( zeros(NP*3, 1) ) ; % delete
gpuArray_RHS_time(myhh) = toc;

tic;
[res]    = feval(entrypoint, rhs_gpu, phib_gpu, v_gpu, nor_gpu, ar_gpu, Up_gpu, NBV, NPV, NP, Nvert, mass_n_GdS);


result1  = gather(res);
RHS_GPU_time(myhh) = toc;

tic;
result2  = [ 
                (dGn_singular(1:NBV) - 1/2).*phib; % singular part for Mb
                -dot(normals(NBV+1:end,:), kron(Up, ones(Nvert, 1) ), 2 ).*G_singular(NBV+1:end)' % singular part for -Hp
           ];
result = result1 + result2; % non-singular part plus singular for (Mb - Hp)
singular_element_of_RHS_time(myhh) = toc;

% if myhh == 1
%     save(['Time_measurement/RHS_CPU_time_NB', int2str(NB), '_NP', int2str(NP), '_N', int2str(NBV+NPV), '.mat'], 'RHS_CPU_time');
%     save(['Time_measurement/RHS_GPU_time_NB', int2str(NB), '_NP', int2str(NP), '_N', int2str(NBV+NPV), '.mat'], 'RHS_GPU_time');
%     save(['Time_measurement/LHS_MVP_CPU_time_NB', int2str(NB), '_NP', int2str(NP), '_N', int2str(NBV+NPV), '.mat'], 'LHS_MVP_CPU_time');
%     save(['Time_measurement/LHS_MVP_GPU_time_NB', int2str(NB), '_NP', int2str(NP), '_N', int2str(NBV+NPV), '.mat'], 'LHS_MVP_GPU_time');
%     save(['Time_measurement/BEM_matrix_time_NB', int2str(NB), '_NP', int2str(NP), '_N', int2str(NBV+NPV), '.mat'], 'BEM_matrix_time');
%     save(['Time_measurement/Jp_matrix_time_NB', int2str(NB), '_NP', int2str(NP), '_N', int2str(NBV+NPV), '.mat'], 'Jp_matrix_time');
%     save(['Time_measurement/singular_element_of_RHS_time_NB', int2str(NB), '_NP', int2str(NP), '_N', int2str(NBV+NPV), '.mat'], 'singular_element_of_RHS_time');
%     save(['Time_measurement/singular_element_of_LHS_time_NB', int2str(NB), '_NP', int2str(NP), '_N', int2str(NBV+NPV), '.mat'], 'singular_element_of_LHS_time');
%     save(['Time_measurement/gpuArray_RHS_time_NB', int2str(NB), '_NP', int2str(NP), '_N', int2str(NBV+NPV), '.mat'], 'gpuArray_RHS_time');
%     save(['Time_measurement/gpuArray_LHS_time_NB', int2str(NB), '_NP', int2str(NP), '_N', int2str(NBV+NPV), '.mat'], 'gpuArray_LHS_time');
%     error(1);
% end
myhh = myhh + 1;

% gpu_time = toc

end

