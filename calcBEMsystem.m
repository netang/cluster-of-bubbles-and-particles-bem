function [qb, phip, RHS, LJ] = calcBEMsystem( G, dGn, normalsp, areasp, Up, phib )
%CALCBEMSYSTEM return qb and phip for RHS

%------ global variables ----------------
global NBV NB NP NPV
% NB - number of bubbles
% NP - number of particles
% NBV - number of vertices all bubbles
% NPV - number of vetiices all particles
global Nvert
%Nvert - number of vertices in one mesh
global rho_l m_p
%----- variable for time measurement ----
global myhh
global RHS_CPU_time
global LHS_MVP_CPU_time
global Jp_matrix_time
%----------------------------------------


% ==============================================================================
tic;
Qp = zeros(NBV+NPV, NPV);
for i = 1:NBV+NPV
    for k = 1:NP
            Np1 = 0;
            Np2 = 0;
            Np3 = 0;        
        for j = (k-1)*Nvert+1:k*Nvert
            Np1 = Np1 + G(i, j+NB*Nvert)*normalsp(j,1);
            Np2 = Np2 + G(i, j+NB*Nvert)*normalsp(j,2);
            Np3 = Np3 + G(i, j+NB*Nvert)*normalsp(j,3);
        end
        for j = (k-1)*Nvert+1:k*Nvert
                   Qp(i,j) = rho_l/m_p*areasp(j)*( Np1*normalsp(j,1) + Np2*normalsp(j,2) + Np3*normalsp(j,3) );
        end        
    end
end
Mb = dGn(:, 1:NBV);
Mp = dGn(:, NBV+1:end); % M without phi(x), dGn consist dS(x)

Jp = Mp - Qp;
Jp_matrix_time(myhh) = toc;
% ==============================================================================


% ==============================================================================
tic;
% ---- CODE PART FOR CALC Hp matrix: {Np = n*G*dS(x)} scalar product with {Up} --------------------------
G_n1_Up1 = G(:,NB*Nvert+1:end).* repmat(normalsp(:,1)', (NBV+NPV), 1) .* kron( Up(:,1)', ones(1, Nvert) );
G_n2_Up2 = G(:,NB*Nvert+1:end).* repmat(normalsp(:,2)', (NBV+NPV), 1) .* kron( Up(:,2)', ones(1, Nvert) );
G_n3_Up3 = G(:,NB*Nvert+1:end).* repmat(normalsp(:,3)', (NBV+NPV), 1) .* kron( Up(:,3)', ones(1, Nvert) );
% -------------------------------------------------------------------------------------------------------
Hp = sum( G_n1_Up1 + G_n2_Up2 + G_n3_Up3, 2);

RHS = (Mb - [1/2*eye(NBV, NBV); zeros(NPV, NBV)])*phib - Hp; % Variable for M_b + 1/2*I*[phi_b] - H_p matrices for bubble and particle
RHS_CPU_time(myhh) = toc;
% ==============================================================================


Lb = G(:, 1:NBV);

LJ = [Lb  -[Jp(1:NBV,:); Jp(NBV+1:end, :)-1/2*eye(NPV, NPV)] ];  % LEFT HAND SIDE matrix

qb_phip = LJ\RHS;

tic;
% LJ = LJ*ones(NBV+NPV, 1);
LHS_MVP_CPU_time(myhh) = toc;



qb = qb_phip(1:NBV);
phip = qb_phip(NBV+1:end);


end

