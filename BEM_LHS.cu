#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

__global__ void BEM_RHS(      double * result, 
                        const double * x, 
                        const double * vertices,
                        const double * normals,
                        const double * areas,
                        const double rho_l_div_m_p,
                        const int NBvert,
                        const int NPvert,
                        const int NP,
                        const int Nvert)  
{
    int indx = blockIdx.x * blockDim.x + threadIdx.x;
    int i, i3, j;

    double v1, v2, v3, y1, y2, y3, d, c, n1, n2, n3, scalar_product_rn, GdS, sum1, sum2, sum3, Np1, Np2, Np3;
    
    c = 1/(4*M_PI);
/* -------------------- */
    if(indx < (NBvert + NPvert) )
    {
        result[indx] = 0.0;
        y1 = vertices[indx*3];
        y2 = vertices[indx*3+1];
        y3 = vertices[indx*3+2];
        for( i = 0; i < NBvert; i++  )  /* {i} for bubbles Lb */
        {
            if(i == indx) continue;
            v1 = y1 - vertices[i*3];
            v2 = y2 - vertices[i*3+1];
            v3 = y3 - vertices[i*3+2];
            d = rsqrt(v1*v1 + v2*v2 + v3*v3);
            result[indx] += x[i]*c*d*areas[i];
        }
        for( j = 0; j < NP; j++ )
        {
            sum1 = 0.0; sum2 = 0.0; sum3 = 0.0;
            Np1  = 0.0; Np2  = 0.0; Np3  = 0.0;
            for( i = NBvert+j*Nvert; i < NBvert+(j+1)*Nvert; i++ )
            {
                i3 = i*3;
                n1 = normals[i3  ];
                n2 = normals[i3+1];
                n3 = normals[i3+2];
                sum1 += x[i]*n1*areas[i];
                sum2 += x[i]*n2*areas[i];
                sum3 += x[i]*n3*areas[i];

                if(i == indx) continue;

                v1 = y1 - vertices[i3];
                v2 = y2 - vertices[i3+1];
                v3 = y3 - vertices[i3+2];
                d = rsqrt(v1*v1 + v2*v2 + v3*v3);
                GdS = c*d*areas[i];
                Np1 += n1*GdS;
                Np2 += n2*GdS;
                Np3 += n3*GdS;  

                
                scalar_product_rn = (v1*n1 + v2*n2 + v3*n3);
                result[indx] -= x[i]*c*d*d*d*scalar_product_rn*areas[i]; /* -Mp */

            }
            result[indx] += rho_l_div_m_p*( Np1*sum1 + Np2*sum2 + Np3*sum3 );  
              /* rho_l/m_p can be multiplicate letter */
        }
    }
/* -------------------- */
    
 
    
}

