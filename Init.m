frec = 2; % frequency of discretizations of mesh
[fF, vF] = SphereMeshZ(frec, 1);

Bc = zeros(0, 3);
ParticleCenters = zeros(0, 3);
BubbleRadius = zeros(1, 0);
ParticleRadius = zeros(1, 0);

% [Bc, ParticleCenters, BubbleRadius, ParticleRadius] = GenerateBPposition_for_3x3x3scheme(r0, 8*r0);

% Bc = zeros(0, 3);
% ParticleCenters = zeros(0, 3);
% BubbleRadius = zeros(1, 0);
% ParticleRadius = zeros(1, 0);
for i = 0:3
    Bc = [Bc; [(4*i)*r0    0     0]];
    ParticleCenters = [ParticleCenters; [(4*i)*r0    7*r0     0]];
    BubbleRadius   = [ParticleRadius r0];
    ParticleRadius = [ParticleRadius r0];
end
% Bc = [0 0 0];

% 
% ParticleCenters = [ 5*r0         0       0;
%                      15*r0         0       0;
%                      10*r0         -2*r0       0;
%                      0          4*r0      0;
%                      0          -10*r0      0;
%                      0          13*r0      0;
%                      5*r0          13*r0      0;
%                      -7*r0          13*r0      0;
%                    -15*r0          13*r0      0;
%                    25*r0         0       0;
%                    35*r0         0       0;
%                    15*r0         15*r0       3*r0];
%                    4*r0      4*r0       6*r0; ];
%                     [ 0   -2.5*r0   0; % zeros(0, 3); % Particle centers for initial time
%                    5*r0     0*r0  0.5*r0];
% BubbleRadius   = [r0 r0 r0 r0 r0 r0 r0 r0 r0 r0 r0 r0]; % радиусы пузырьков, соответственно.
% ParticleRadius = [r0 r0 r0 r0 r0 r0 r0 r0 r0 r0 r0 r0]; % радиусы частиц, соответственно.

[facesb, verticesb, facesp, verticesp, markFaces, markVertices, Nvert] = ...
    BubbleParticleMesh( Bc, ParticleCenters, BubbleRadius, ParticleRadius, frec );
faces = [facesb; facesp + size(verticesb, 1) ];


% All vertices = [verticesb; verticesp]

% Nvert - numbers of vertices in one mesh
NB = size(Bc, 1);              % number of bubbles
NP = size(ParticleCenters, 1); % number of particles
NBV = NB * Nvert;
NPV = NP * Nvert;

% centers of particles in local coordinates
rp_local_vert = verticesp - kron(ParticleCenters, ones( Nvert, 1) );
rb = verticesb;
Rp = ParticleCenters;
Up = zeros(NP, 3);


m_p = rho_p*(4/3)*pi*r0.^3;

% timing
tmin = 0;
tmax = tmax0;
ht = ht0; 
ntf = fout;
nts = fsave; % 
nt = round( (tmax-tmin)/ht )+1; tmax = ht*(nt-1); 
ts = ht*linspace(0,nt-1,nt);
touts = ts(1:ntf:nt); nout = length(touts)+1;
touts(nout)=touts(nout-1)+ntf*ht;
tsaves=ts(1:nts:nt); nsave=length(tsaves)+1;
tsaves(nsave) = tsaves(nsave-1)+nts*ht;
hh = 1;
ht = ht0;
tmax = tmax0; % 
myhh = 1;


% mesh constant parameters
[AllNeighbors, AllNeighborFaces, NeiBookmarks] = ...
    SetVertexNeighborFaceDataStructure( faces );

[Pg0, phog0, mg0] = get_init_parameter( rb, BubbleRadius );


[normalsb, areasb] = get_normals_areas(rb, facesb);
[Vg0] = get_volume(rb, normalsb, areasb);
[Filter] = matrix_filter(vF);
[coef_ab_n] = get_coef_ab_n(odemem,type_scheme);



phib = zeros(length(rb),1);              % potential value on bubbles nodes
[Bc] = get_centers(rb, areasb);          % centers of bubbles
[Vg] = get_volume(rb, normalsb, areasb); % volumes of bubbles



