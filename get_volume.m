function [Vol] = get_volume(v, normals, areas)

global NB Bc Nvert

Vol = zeros(NB, 1);
r = v - kron(Bc, ones(Nvert, 1));
rn = (dot(r',normals'))';
volume = rn.*areas/3;
for bub = 1:NB
     Vol(bub) = sum(volume(Nvert*(bub-1)+1:Nvert*bub));
end;