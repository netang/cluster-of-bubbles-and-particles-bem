
j = 1;
result_table = zeros(9, 11);
result = zeros(100, 5);
% LHS_N_time = zeros(4, 2);
for k = 1:125

    if exist ( ['RHS_CPU_time_NB', int2str(k), '_NP', int2str(k), '_N', int2str(2*k*162), '.mat'] )
        disp(['����� � k = ',  int2str(k)] );
        load(['RHS_CPU_time_NB', int2str(k), '_NP', int2str(k), '_N', int2str(2*k*162), '.mat']); % 162 is number of vertices in one mesh
        load(['RHS_GPU_time_NB', int2str(k), '_NP', int2str(k), '_N', int2str(2*k*162), '.mat']); % 162 is number of vertices in one mesh
        load(['LHS_MVP_CPU_time_NB', int2str(k), '_NP', int2str(k), '_N', int2str(2*k*162), '.mat']);
        load(['LHS_MVP_GPU_time_NB', int2str(k), '_NP', int2str(k), '_N', int2str(2*k*162), '.mat']); 
         load(['BEM_matrix_time_NB', int2str(k), '_NP', int2str(k), '_N', int2str(2*k*162), '.mat']);
         load(['Jp_matrix_time_NB', int2str(k), '_NP', int2str(k), '_N', int2str(2*k*162), '.mat']);
         load(['singular_element_of_RHS_time_NB', int2str(k), '_NP', int2str(k), '_N', int2str(2*k*162), '.mat']);
         load(['singular_element_of_LHS_time_NB', int2str(k), '_NP', int2str(k), '_N', int2str(2*k*162), '.mat']);
        load(['gpuArray_RHS_time_NB', int2str(k), '_NP', int2str(k), '_N', int2str(2*k*162), '.mat']);
        load(['gpuArray_LHS_time_NB', int2str(k), '_NP', int2str(k), '_N', int2str(2*k*162), '.mat']);

        result(j, 1) = 2*k*162;
        result(j, 2) = mean(RHS_CPU_time) + mean(BEM_matrix_time)/2 + mean(Jp_matrix_time) ;
        result(j, 3) = mean(RHS_GPU_time) + mean(gpuArray_RHS_time); % + mean(singular_element_of_RHS_time) + mean(singular_element_of_LHS_time);
        result(j, 4) = mean(LHS_MVP_CPU_time)+  mean(BEM_matrix_time)/2;
        result(j, 5) = mean(LHS_MVP_GPU_time) + mean(gpuArray_LHS_time);
        
%         result_table(j, 1) = 2*k*162;
%         result_table(j, 2) = RHS_CPU_time;
%         result_table(j, 3) = RHS_GPU_time;
%         result_table(j, 4) = LHS_MVP_CPU_time;
%         result_table(j, 5) = LHS_MVP_GPU_time;
%         result_table(j, 6) = BEM_matrix_time;
%         result_table(j, 7) = Jp_matrix_time;
%         result_table(j, 8) = singular_element_of_RHS_time;
%         result_table(j, 9) = singular_element_of_LHS_time;
%         result_table(j, 10) = gpuArray_RHS_time;
%         result_table(j, 11) = gpuArray_LHS_time;
        j = j + 1;
    end
end


figure;
loglog(result(1:j-1,1), result(1:j-1,2), '-o', result(1:j-1,1), result(1:j-1,3), '-s');%
legend('CPU', 'GPU');
xlabel('���������� ��������� �����');
ylabel('����� �������');
grid minor;

figure;
loglog(result(1:j-1,1), result(1:j-1, 4), '-o', result(1:j-1,1), result(1:j-1, 5), '-s');
legend('CPU', 'GPU');
xlabel('���������� ��������� �����');
ylabel('����� �������');
grid minor