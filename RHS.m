function [drb, dphib, dRp, dUp] = RHS(time, rb, phib, Rp, Up)
%function [drb, dphib, dRp, dUp, dDtp, dMp] = RHS(time, rb, phib, Rp, Up, DTp, systemMp) % Mp already exist
% right-hand side computation
% input:   rb,        phib,        Rp,       Up         (+ time)
% output:  d(rb)/dt   d(phib)/dt   d(Rp)/dt  d(Up)/dt


% size_rb = size(rb)
% size_phib = size(phib)
% sizeRp = size(Rp)
% sizeUp = size(Up)
% error(1)


global facesb facesp faces
% facesb - all bubbles faces
% facesp - all particles faces
% faces  - all faces

% global parameters: g, rho_l, rho_p[], kappa, sigma, k
global sigma rho_l g alpha m_p rho_p
global Pg0 kappa omega Ap P0 Filter

global rp_local_vert Nvert
% Nvert - Number of vertices in one mesh

global Vg Vg0

global NBV NB NP NPV
% NB - number of bubbles
% NP - number of particles
% NBV - number of vertices all bubbles

% global debug_i

for i = 1:NB
    k = (Nvert*(i-1)+1:Nvert*i)';
    phib(k) = Filter*phib(k);
    rb(k,1) = Filter*rb(k,1);
    rb(k,2) = Filter*rb(k,2);
    rb(k,3) = Filter*rb(k,3);
end;

rp = rp_local_vert + kron(Rp, ones( Nvert, 1) ); % determine current mesh for particles with help of Rp;

[normalsb, areasb] = get_normals_areas(rb, facesb);

% normalsp = zeros(0, 3);
% areasp   = zeros(0, 1);
[normalsp, areasp] = get_normals_areas(rp, facesp);


n       = [normalsb; normalsp]; % �������� �� normals ����, ������.
areas   = [areasb;   areasp];

v = [rb; rp];


[G, dGn] = get_bem_matrices(v, n, areas); % dGn is dG(x, y)/dn(n)*area, A is G(x, y)*area


[curvb, normalsb] = get_surface_curvature(rb, normalsb);


[w] = get_tangential_velocity(facesb, rb, phib, areasb, normalsb);


nor_ar_p(:,1) = normalsp(:,1).*areasp;
nor_ar_p(:,2) = normalsp(:,2).*areasp;
nor_ar_p(:,3) = normalsp(:,3).*areasp;


[qb, phip] = calcBEMsystem(G, dGn, normalsp, areasp, Up, phib);

% -------- PARALLEL PART. BEFORE THIS GO IN RHS_GPU.m -> -------------------
% -------- Delete this block of code if the RHS_GPU.m is working correctly.
% [dGn_singular] = get_singular_dGn(v, n, areas);
% [G_singular]   = get_singular_G(v, n, areas, dGn_singular);
% % 
% % % rhs = get_BEM_rhs(phib, v, n, areas, dGn_singular); % this is BEM rhs
% % Overload_Multiplication_Operator([ones(NBV+NPV, 1)], v, n, areas, G_singular, dGn_singular);
% RHS_on_GPU = get_BEM_rhs(phib, v, n, areas, dGn_singular, G_singular, Up); % this is BEM rhs
% % % Hp_GPU = get_Hp_GPU(v, n, areas, G_singular); % this is BEM rhs
% % ------------------- For debug tests. Can be deleted --------
% % % % xxx = rand(NBV+NPV, 1)*100;
% % % % LJ = LJ*xxx;
% % % % LJ_GPU = Overload_Multiplication_Operator(xxx, v, n, areas, G_singular, dGn_singular); %[ones(NBV+NPV, 1)]
% % ------------------------------------------------------------
% % % h = @(x)Overload_Multiplication_Operator(x, v, areas, G_singular, dGn_singular);
% h = P3DBL_get_mult_G_handle_GPU(v, n, areas, G_singular, dGn_singular);
% % % tic
% qb_phip_GPU = gmres(h, RHS_on_GPU, 30, 1e-6, 10); % vpn=gmres(h,rhs,30,1e-6,10); 
% % % cpu_gpu_gmres = toc
% qb_GPU   = qb_phip_GPU(1:NBV);
% phip_GPU = qb_phip_GPU(NBV+1:end);
% % 
% % size(Lb)
% % size(Lb_GPU)
% 
% %    first  = max(abs( diag(G) - G_singular' ))/max( abs(G_singular) ) 
% %     RHS_relative_error  = max(abs(RHS_on_GPU-RHS_CPU ))/max( abs(RHS_on_GPU) )
% %     LJ_relative_error   = max(abs(LJ_GPU-LJ ))/max( abs(LJ) )
%       qb_relative_error   = max(abs(qb_GPU-qb ))/max( abs(qb) )
%       phip_relative_error = max(abs(phip_GPU-phip ))/max( abs(phip) )
% %    second = max(abs(LJ_GPU-LJ ))
% % -------------------------------------


[wp] = get_tangential_velocity(facesp, rp, phip, areasp, normalsp);


drb(:,1) = qb.*normalsb(:,1)+(1+alpha)*w(:,1);
drb(:,2) = qb.*normalsb(:,2)+(1+alpha)*w(:,2);
drb(:,3) = qb.*normalsb(:,3)+(1+alpha)*w(:,3);



nabla_phi_square = dot(drb',drb')';
ww = dot(w', drb')';

Pa = -Ap*sin(omega*time);
% Pa = -Ap*sin(omega*time)^2;
Pinf = P0 + Pa;


% [Vg] = P3DBL_get_volume_all(rb);
[Vg] = get_volume(rb, normalsb, areasb);
%part_phip =  kron(((Vg0./Vg).^kappa), ones(Nvert, 1)); %   kron(Pg0'.*((Vg0./Vg).^kappa), ones(Nvert, 1));
dphib = (nabla_phi_square/2+alpha*ww + ...
        ( Pinf - ...
        kron(Pg0'.*((Vg0./Vg).^kappa), ones(Nvert, 1)) + ...
        2*sigma*curvb)/rho_l-rb(:,3)*g);
    
dRp = zeros(0, 3);
dUp = zeros(0, 3);
if NP ~= 0
    phip_n_ar = [nor_ar_p(:,1).*phip ...
                 nor_ar_p(:,2).*phip ...
                 nor_ar_p(:,1).*phip];
    phip_n_ar = reshape(phip_n_ar, Nvert, NP*3);
    phip_n_ar = sum(phip_n_ar, 1);
    phip_n_ar = vec2mat(phip_n_ar, NP)';
    end

    if NP ~= 0
    dRp = Up + rho_l/m_p*phip_n_ar;
    end

    if NP ~= 0
    normalsp_Up = sum( normalsp .* kron(Up, ones(Nvert,1)), 2 );
    normalsp_psy = sum( normalsp .* kron(phip_n_ar, ones(Nvert, 1)), 2 );

    qp = rho_l/m_p * normalsp_psy + normalsp_Up;


    vpp1(:,1)=qp.*normalsp(:,1)+wp(:,1);
    vpp1(:,2)=qp.*normalsp(:,2)+wp(:,2);
    vpp1(:,3)=qp.*normalsp(:,3)+wp(:,3);


    delta_phip_n_S = repmat(dot(vpp1',vpp1'), 3, 1)' .* nor_ar_p;
    delta_phip_n_S = reshape(delta_phip_n_S, Nvert,  NP*3);
    delta_phip_n_S = sum(delta_phip_n_S);
    delta_phip_n_S = vec2mat(delta_phip_n_S, NP)';

    q_delata_phip = repmat(dot(nor_ar_p',vpp1'), 3, 1)' .* vpp1;
    q_delata_phip = reshape(q_delata_phip, Nvert,  NP*3);
    q_delata_phip = sum(q_delata_phip);
    q_delata_phip = vec2mat(q_delata_phip, NP)';

    dUp = repmat( (1 - rho_l/rho_p)*[0 0 g], NP, 1) + ...
       rho_l/m_p * ( (1/2)*delta_phip_n_S ...
       - q_delata_phip ) ;                                       
    end

% dMp = 

for i = 1:NB
    k=(Nvert*(i-1)+1:Nvert*i)';
    dphib(k) = Filter*dphib(k);
    drb(k,1) = Filter*drb(k,1);
    drb(k,2) = Filter*drb(k,2);
    drb(k,3) = Filter*drb(k,3);
end;

% save(['new_bem' int2str(debug_i) '.mat']);
% debug_i
% if debug_i == 6
%     error('1')
% end
% debug_i = debug_i + 1;
% drb
% dphib
% dRp
% dUp

end

