function [Gs] = get_singular_G(v,normals,areas,Gns)
global S;
global myhh singular_GS_time
tic;
Nvert=length(v);
Nvert3=Nvert*3;
C=zeros(3,Nvert);
Gs=zeros(1,Nvert);
G1=zeros(1,Nvert);
G2=zeros(1,Nvert);
G3=zeros(1,Nvert);
Gn1=zeros(1,Nvert);
Gn2=zeros(1,Nvert);
Gn3=zeros(1,Nvert);
f=ones(Nvert,1);         
result=zeros(Nvert,1);
v_gpu=reshape(v',1,Nvert3);
norm_gpu=reshape(normals',1,Nvert3);
N=Nvert;

%G*n
entrypointG = parallel.gpu.CUDAKernel('mvproductG.ptx', 'mvproductG.cu');
entrypointG.ThreadBlockSize=S;
entrypointG.GridSize = ceil(N/S);

v_gpu=gpuArray(v_gpu);
ar_gpu=gpuArray(areas);
nor_gpu=gpuArray(norm_gpu);

tempG1 = zeros(Nvert,1);
tempG1=gpuArray(tempG1);
f=gpuArray(normals(:,1));

[tempG1] = feval(entrypointG, tempG1, f, v_gpu, ar_gpu, N);
G1=gather(tempG1);

tempG2 = zeros(Nvert,1);
tempG2=gpuArray(tempG2);
f=gpuArray(normals(:,2));

[tempG2] = feval(entrypointG, tempG2, f, v_gpu, ar_gpu, N);
G2=gather(tempG2);

tempG3 = zeros(Nvert,1);
tempG3=gpuArray(tempG3);
f=gpuArray(normals(:,3));

[tempG3] = feval(entrypointG, tempG3, f, v_gpu, ar_gpu, N);
G3=gather(tempG3);

%Gn*v
entrypointGn = parallel.gpu.CUDAKernel('mvproductGn.ptx', 'mvproductGn.cu');
entrypointGn.ThreadBlockSize=S;
entrypointGn.GridSize = ceil(N/S);

tempGn1 = zeros(Nvert,1);
tempGn1=gpuArray(tempGn1);
f=gpuArray(v(:,1));

[tempGn1] = feval(entrypointGn, tempGn1, f, v_gpu, nor_gpu, ar_gpu, N);
Gn1=gather(tempGn1);

tempGn2 = zeros(Nvert,1);
tempGn2=gpuArray(tempGn2);
f=gpuArray(v(:,2));

[tempGn2] = feval(entrypointGn, tempGn2, f, v_gpu, nor_gpu, ar_gpu, N);
Gn2=gather(tempGn2);

tempGn3 = zeros(Nvert,1);
tempGn3=gpuArray(tempGn3);
f=gpuArray(v(:,3));

[tempGn3] = feval(entrypointGn, tempGn3, f, v_gpu, nor_gpu, ar_gpu, N);
Gn3=gather(tempGn3);

C(1,:)=G1'-Gn1'-0.5*(v(:,1))'-(Gns.*v(:,1))';
C(2,:)=G2'-Gn2'-0.5*(v(:,2))'-(Gns.*v(:,2))';
C(3,:)=G3'-Gn3'-0.5*(v(:,3))'-(Gns.*v(:,3))';

for n=1:Nvert 
   Gs(n)=-(normals(n,:))'\C(:,n);
end;

singular_GS_time(myhh) = toc;