function [ output_args ] = get_Hp_GPU( vertices, normals, areas, G_singular )
%GET_HP_GPU Summary of this function goes here
%   Detailed explanation goes here
global NBV NPV
global S

% rhs     = zeros(NBV + NPV,1);
result  = zeros(NBV + NPV,1);
result1 = zeros(NBV + NPV,1);
result2 = zeros(NBV + NPV,1);

% v_gpu    = reshape(vertices', 1, NBV*3);
nor_gpu  = reshape(normals', 1, NBV*3);

entrypoint = parallel.gpu.CUDAKernel('BEM_RHS.ptx', 'BEM_RHS.cu');
entrypoint.ThreadBlockSize = S;
entrypoint.GridSize = ceil( (NBV + NPV)/S );

% rhs_gpu  = gpuArray(rhs);
% v_gpu    = gpuArray(v_gpu);
ar_gpu   = gpuArray(areas);
nor_gpu  = gpuArray(nor_gpu);
% phib_gpu = gpuArray(phib);

[res]    = feval(entrypoint, rhs_gpu, phib_gpu, v_gpu, nor_gpu, ar_gpu, NBV, NPV);

result1  = gather(res);
            
result2  = [ (dGn_singular(1:NBV)+1/2).*phib(1:NBV);
              dGn_singular(NBV+1:end).*phib(NBV+1:end);
            ];

result = result1+result2;
% result = -phib/2+result1+result2;


end

