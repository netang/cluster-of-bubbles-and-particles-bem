handleGraphic = figure();
mTextBox = uicontrol('style','text');
mTextBox.Position = [230 390 25 20];
set(mTextBox,'String','time: ');
mTextBoxForTime = uicontrol('style','text');
mTextBoxForTime.Position = [260 390 45 20];
    
h = 1;
it = 1;
time = 0; 

% Variables for saving calculation date
% ------------------------------------------
% VerticesAllTime = zeros(nt-1, NPV+NBV, 3);
Vg_mas = zeros(nt-1, NB);
Rp_mas = zeros(nt-1, NP, 3);
Up_mas = zeros(nt-1, NP, 3);
Bc_mas = zeros(nt-1, NB, 3);
time_mas = zeros(nt-1, 1);
% % % RHS_CPU_time = zeros(1, 1); % 10 is number of iteration for saving
% % % RHS_GPU_time = zeros(1, 1);
% % % LHS_MVP_CPU_time = zeros(1, 1);
% % % LHS_MVP_GPU_time = zeros(1, 1);
% % % BEM_matrix_time = zeros(1, 1);
% % % Jp_matrix_time = zeros(1, 1);
% % % singular_element_of_RHS_time = zeros(1, 1);
% % % singular_element_of_LHS_time = zeros(1, 1);
% % % gpuArray_RHS_time = zeros(1, 1);
% % % gpuArray_LHS_time = zeros(1, 1);
% ------------------------------------------

Visualize;

isave=1;

if multistep_mem > 1
    sizev = size(rb); 
    lszv = length(sizev); 
    sizev(lszv+1) = multistep_mem-1;
    vmem = zeros(sizev);
    phimem = zeros(length(rb),multistep_mem-1);
    Rpmem = zeros(NP, 3, multistep_mem-1);
    Upmem = zeros(NP, 3, multistep_mem-1);
end;
           

tcpu0 = cputime;
it0 = it;


switch CPU_or_GPU
    case 1
            disp('Calc on CPU');
            while it < nt    %nt - количество временных отрезков = (time/step); it - текущий номер шага

                tcpu = cputime;

                if multistep_mem > 1
                    if it < multistep_mem % типо для начала просчитываем методом Рунге-Кутта, чтобы получить несколько значений производных для метода Адамса
                        [rb, drb, phib, dphib, Rp, dRp, Up, dUp] = RungeKutta4(time, rb, phib, Rp, Up);
                        mempos = multistep_mem-it;
                        vmem(:,:,mempos) = drb;
                        phimem(:,mempos) = dphib;
                        Rpmem(:, :, mempos) = dRp;
                        Upmem(:, :, mempos) = dUp;

                    else
                        if type_scheme == 1 % adamsa bashforta
            %                 disp('считает Адамсом-Башфортом');
                                      [rb, vmem, phib, phimem, Rp, Rpmem, Up, Upmem] = ...
                            ab_n(time, rb, vmem, phib, phimem, Rp, Rpmem, Up, Upmem);  
                        else

                                       [rb, vmem, phib, phimem, Rp, Rpmem, Up, Upmem] = ...
                             abm6(time, rb, vmem, phib, phimem, Rp, Rpmem, Up, Upmem);
            %                 disp('считает Адамсом-Башфортом-Молтоном');
                        end;

                    end;
                else
                    [rb, drb, phib, dphib, Rp, dRp, Up, dUp] = RungeKutta4(time, rb, phib, Rp, Up);
            %         disp('считае всегда просто Рунге-Куттом');
                end;

                Visualize;
                CalcDataWriteToArray;

                it = it + 1; 
                time = (it-1)*ht;

                %----------------------------------------------------------------------
                %-------------------------просто вывод времени-------------------------
                steptime = cputime-tcpu;
                passedtime = cputime-tcpu0;
                lefttime = passedtime*(nt-it)/(it-it0);
                step = time/ht0;
                fprintf('step = %f, time = %f,  cputime step = %f, cputime passed = %f, cputime left = %f\n',...
                         step,      time,       steptime,          passedtime,          lefttime);        
                %----------------------------------------------------------------------

            end;
    case 2
        disp('code for calculation on GPU no write still.')
            while it < nt    %nt - количество временных отрезков = (time/step); it - текущий номер шага

                tcpu = cputime;

                if multistep_mem > 1
                    if it < multistep_mem % типо для начала просчитываем методом Рунге-Кутта, чтобы получить несколько значений производных для метода Адамса
                        [rb, drb, phib, dphib, Rp, dRp, Up, dUp] = RungeKuttaGPU(time, rb, phib, Rp, Up);
                        mempos = multistep_mem-it;
                        vmem(:,:,mempos) = drb;
                        phimem(:,mempos) = dphib;
                        Rpmem(:, :, mempos) = dRp;
                        Upmem(:, :, mempos) = dUp;

                    else
                        if type_scheme == 1 % Adams-Bashfort
                                                   [rb, vmem, phib, phimem, Rp, Rpmem, Up, Upmem] = ...
                            AdamsBashforthGPU(time, rb, vmem, phib, phimem, Rp, Rpmem, Up, Upmem);  
                        else
                                                          [rb, vmem, phib, phimem, Rp, Rpmem, Up, Upmem] = ...
                            AdamsBashforthMoultonGPU(time, rb, vmem, phib, phimem, Rp, Rpmem, Up, Upmem);
                        end;

                    end;
                else % Calculation with Runge-Kutta method
                    [rb, drb, phib, dphib, Rp, dRp, Up, dUp] = RungeKuttaGPU(time, rb, phib, Rp, Up);
                end;

                Visualize;
                CalcDataWriteToArray;
                
                it = it + 1; 
                time = (it-1)*ht;

                %----------------------------------------------------------------------
                %-------------------------просто вывод времени-------------------------
                steptime = cputime-tcpu;
                passedtime = cputime-tcpu0;
                lefttime = passedtime*(nt-it)/(it-it0);
                step = time/ht0;
                fprintf('step = %f, time = %f,  cputime step = %f, cputime passed = %f, cputime left = %f\n',...
                         step,      time,       steptime,          passedtime,          lefttime);        
                %----------------------------------------------------------------------

            end;
    otherwise
        disp('FMM method')
end

% main time loop
% why while loop? i can do it by use loop for
