set(mTextBoxForTime, 'String', num2str(time));
set(0, 'DefaultAxesFontSize', 14);

rp = rp_local_vert + kron(Rp, ones( Nvert, 1) );

v = [rb; rp]; % all vertices

trimesh(facesb, rb(:,1)/r0, rb(:,2)/r0, rb(:,3)/r0, 'Facecolor', 'blue', 'EdgeColor', 'black', 'LineWidth', 1, 'FaceAlpha', 0.5); lighting phong;
% trimesh(facesb, rb(:,1)/r0, rb(:,2)/r0, rb(:,3)/r0, 'FaceLighting', 'gouraud', 'BackFaceLighting', 'reverselit', 'Facecolor', 'blue');
hold on;
trimesh(facesp, rp(:,1)/r0, rp(:,2)/r0, rp(:,3)/r0, 'Facecolor', 'red', 'EdgeColor', 'black', 'LineWidth', 1, 'FaceAlpha', 0.2);
% trimesh(facesp, rp(:,1)/r0, rp(:,2)/r0, rp(:,3)/r0);
hold off;
% trimesh(faces, v(:,1), v(:,2), v(:,3), 'Facecolor', 'red', 'EdgeColor', 'black', 'LineWidth', 1, 'FaceAlpha', 0.2); lighting phong;
% view([0 90]);
view([55 30]);
% title('Bubbles and particles interaction');
xlabel('x'''); ylabel('y'''); zlabel('z''');
axis equal;

c = 4;
xmin = -(c+7); xmax = (c+7);
ymin = -(c+7); ymax = (c+7);
zmin = -(c+7); zmax = (c+7);

xlim([xmin xmax]); 
ylim([ymin ymax]);
zlim([zmin zmax]);




drawnow;