function [drb, dphib, dRp, dUp] = RHS_GPU(time, rb, phib, Rp, Up)
%function [drb, dphib, dRp, dUp, dDtp, dMp] = RHS(time, rb, phib, Rp, Up, DTp, systemMp) % Mp already exist
% right-hand side computation
% input:   rb,        phib,        Rp,       Up         (+ time)
% output:  d(rb)/dt   d(phib)/dt   d(Rp)/dt  d(Up)/dt


% size_rb = size(rb)
% size_phib = size(phib)
% sizeRp = size(Rp)
% sizeUp = size(Up)
% error(1)


global facesb facesp faces
% facesb - all bubbles faces
% facesp - all particles faces
% faces  - all faces

% global parameters: g, rho_l, rho_p[], kappa, sigma, k
global sigma rho_l g alpha m_p rho_p
global Pg0 kappa omega Ap P0 Filter

global rp_local_vert Nvert
% Nvert - Number of vertices in one mesh

global Vg Vg0

global NBV NB NP NPV
% NB - number of bubbles
% NP - number of particles
% NBV - number of vertices all bubbles

% global debug_i

for i = 1:NB
    k = (Nvert*(i-1)+1:Nvert*i)';
    phib(k) = Filter*phib(k);
    rb(k,1) = Filter*rb(k,1);
    rb(k,2) = Filter*rb(k,2);
    rb(k,3) = Filter*rb(k,3);
end;

rp = rp_local_vert + kron(Rp, ones( Nvert, 1) ); % determine current mesh for particles with help of Rp;

[normalsb, areasb] = get_normals_areas(rb, facesb);

% normalsp = zeros(0, 3);
% areasp   = zeros(0, 1);
[normalsp, areasp] = get_normals_areas(rp, facesp);


n       = [normalsb; normalsp]; % �������� �� normals ����, ������.
areas   = [areasb;   areasp];

v = [rb; rp];


[curvb, normalsb] = get_surface_curvature(rb, normalsb);


[w] = get_tangential_velocity(facesb, rb, phib, areasb, normalsb);


nor_ar_p(:,1) = normalsp(:,1).*areasp;
nor_ar_p(:,2) = normalsp(:,2).*areasp;
nor_ar_p(:,3) = normalsp(:,3).*areasp;





[dGn_singular] = get_singular_dGn(v, n, areas);
[G_singular]   = get_singular_G(v, n, areas, dGn_singular);

RHS_on_GPU = get_BEM_rhs(phib, v, n, areas, dGn_singular, G_singular, Up); % this is BEM rhs

h = P3DBL_get_mult_G_handle_GPU(v, n, areas, G_singular, dGn_singular);

qb_phip_GPU = gmres(h, RHS_on_GPU, 30, 1e-6, 10);

qb   = qb_phip_GPU(1:NBV);
phip = qb_phip_GPU(NBV+1:end);




[wp] = get_tangential_velocity(facesp, rp, phip, areasp, normalsp);


drb(:,1) = qb.*normalsb(:,1)+(1+alpha)*w(:,1);
drb(:,2) = qb.*normalsb(:,2)+(1+alpha)*w(:,2);
drb(:,3) = qb.*normalsb(:,3)+(1+alpha)*w(:,3);



nabla_phi_square = dot(drb',drb')';
ww = dot(w', drb')';

Pa = -Ap*sin(omega*time);
% Pa = -Ap*sin(omega*time)^2;
Pinf = P0 + Pa;


% [Vg] = P3DBL_get_volume_all(rb);
[Vg] = get_volume(rb, normalsb, areasb);
%part_phip =  kron(((Vg0./Vg).^kappa), ones(Nvert, 1)); %   kron(Pg0'.*((Vg0./Vg).^kappa), ones(Nvert, 1));
dphib = (nabla_phi_square/2+alpha*ww + ...
        ( Pinf - ...
        kron(Pg0'.*((Vg0./Vg).^kappa), ones(Nvert, 1)) + ...
        2*sigma*curvb)/rho_l-rb(:,3)*g);
    
dRp = zeros(0, 3);
dUp = zeros(0, 3);
if NP ~= 0
    phip_n_ar = [nor_ar_p(:,1).*phip ...
                 nor_ar_p(:,2).*phip ...
                 nor_ar_p(:,1).*phip];
    phip_n_ar = reshape(phip_n_ar, Nvert, NP*3);
    phip_n_ar = sum(phip_n_ar, 1);
    phip_n_ar = vec2mat(phip_n_ar, NP)';
    end

    if NP ~= 0
    dRp = Up + rho_l/m_p*phip_n_ar;
    end

    if NP ~= 0
    normalsp_Up = sum( normalsp .* kron(Up, ones(Nvert,1)), 2 );
    normalsp_psy = sum( normalsp .* kron(phip_n_ar, ones(Nvert, 1)), 2 );

    qp = rho_l/m_p * normalsp_psy + normalsp_Up;


    vpp1(:,1)=qp.*normalsp(:,1)+wp(:,1);
    vpp1(:,2)=qp.*normalsp(:,2)+wp(:,2);
    vpp1(:,3)=qp.*normalsp(:,3)+wp(:,3);


    delta_phip_n_S = repmat(dot(vpp1',vpp1'), 3, 1)' .* nor_ar_p;
    delta_phip_n_S = reshape(delta_phip_n_S, Nvert,  NP*3);
    delta_phip_n_S = sum(delta_phip_n_S);
    delta_phip_n_S = vec2mat(delta_phip_n_S, NP)';

    q_delata_phip = repmat(dot(nor_ar_p',vpp1'), 3, 1)' .* vpp1;
    q_delata_phip = reshape(q_delata_phip, Nvert,  NP*3);
    q_delata_phip = sum(q_delata_phip);
    q_delata_phip = vec2mat(q_delata_phip, NP)';

    dUp = repmat( (1 - rho_l/rho_p)*[0 0 g], NP, 1) + ...
       rho_l/m_p * ( (1/2)*delta_phip_n_S ...
       - q_delata_phip ) ;                                       
    end

 

for i = 1:NB
    k=(Nvert*(i-1)+1:Nvert*i)';
    dphib(k) = Filter*dphib(k);
    drb(k,1) = Filter*drb(k,1);
    drb(k,2) = Filter*drb(k,2);
    drb(k,3) = Filter*drb(k,3);
end;



end

